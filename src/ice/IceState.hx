package ice;

import etwin.flash.MovieClip;
import hf.Hf;
import custom_clips.CustomClips;
import ice.Snowflake;

class IceState {
  public var hf(default, null): Hf;
  public var snowMC(default, null): MovieClip;

  private var customClips: CustomClips;
  private var snow(default, null): Array<Snowflake>;
  private var reserve(default, null): Array<Snowflake>;
  private var density: Float;
  private var speed: Float;
  private var nextChange: Float;

  public function new(hf: Hf, customClips: CustomClips, snowMC: MovieClip): Void {
    this.hf = hf;
    this.snowMC = snowMC;
    this.customClips = customClips;
    this.snow = [];
    this.reserve = [];
    this.density = 30;
    this.speed = 3.5;
    this.nextChange = 100;
  }

  public function onScroll(): Void {
    var GAME_HEIGHT: Float = this.hf.Data.GAME_HEIGHT;
    for (snow in this.snow) {
      var y: Float = snow._y - GAME_HEIGHT;
      if (y < -10) {
        snow._visible = false;
      } else {
        snow._y = y;
      }
    }
    this.compactSnow();
  }

  public function update(): Void {
    this.modulate();

    if (this.hf.Std.random(100) <= this.density) {
      var GAME_WIDTH: Float = this.hf.Data.GAME_WIDTH;
      this.attachSnowflake(
        this.hf.Std.random(Std.int(GAME_WIDTH)),
        -10,
        (20 + this.hf.Std.random(40)) / 60 * this.speed
      );
    }

    var GAME_HEIGHT: Float = this.hf.Data.GAME_HEIGHT;

    for (snow in this.snow) {
      snow._y += snow.speed;
      if (snow._y > 2 * GAME_HEIGHT + 10) {
        snow._visible = false;
      }
    }

    this.compactSnow();
  }

  private function modulate(): Void {
    if (this.nextChange > 0) {
      // TODO: Use tmod
      this.nextChange -= 1;
      return;
    }

    this.density = 5 + this.hf.Std.random(85);
    this.speed = 3 + this.hf.Std.random(300) / 100;
    this.nextChange = 50 + this.hf.Std.random(100);
  }

  private function compactSnow(): Void {
    var i: Int = 0;
    while (i < this.snow.length) {
      var snow: Snowflake = this.snow[i];
      if (snow._visible) {
        i++;
      } else {
        this.snow[i] = this.snow[this.snow.length - 1];
        this.snow.pop();
        this.reserve.push(snow);
      }
    }
  }

  function attachSnowflake(x: Float, y: Float, speed: Float): Snowflake {
    var snow: Snowflake;
    if (this.reserve.length > 0) {
      snow = this.reserve.pop();
    } else {
      snow = customClips.addMovie(this.snowMC, "hammer_fx_particle", Snowflake);
    }
    this.snow.push(snow);

    snow.init(this.hf, x, y, speed);
    snow.gotoAndStop("" + this.hf.Data.PARTICLE_CLASSIC_BOMB);
    snow.sub.gotoAndStop("" + 1);
    return snow;
  }
}
