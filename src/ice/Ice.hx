package ice;

import etwin.flash.filters.BitmapFilter;
import etwin.flash.MovieClip;
import hf.native.BitmapData;

import hf.Hf;
import hf.levels.Data in LevelData;
import hf.levels.View;
import hf.levels.ViewManager;
import hf.mode.GameMode;
import hf.Mode;

import etwin.ds.WeakMap;
import patchman.IPatch;
import etwin.Obfu;
import patchman.PatchList;
import patchman.Ref;
import patchman.HostModuleLoader;

import merlin.IAction;

import custom_clips.CustomClips;
import color_matrix.ColorMatrix;



@:build(patchman.Build.di())
class Ice {
  private static var STATES(default, null): WeakMap<Mode, IceState> = new WeakMap();
  private var listePFIce: Array<MovieClip> = [];
  private static var optionName(default, never): String = Obfu.raw("ice");

  public static function isOptionEnabled(loader: HostModuleLoader): Bool {
    var run = loader.require(Obfu.raw("run")).getRun();
    var options: Array<Dynamic> = run.gameOptions;
    for (i in 0...(options.length)) {
      if (options[i] == optionName)
        return true;
    }
    return false;
  }

  @:diExport
  public var activeOption(default, null) : IPatch;

  @:diExport
  public var iceTrigger(default, null): IAction;
  // Force `ICE` dynamic var
  private static var FORCE_ICE_VAR(default, null): IPatch =
    Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
      if (isIce(self)) {
        self.setDynamicVar(Obfu.raw("ICE"), 1);
      }
    });

  //
  private static function getBlue(){
    var matrix = new Array();
    var c = 1/2;
    matrix = matrix.concat([c, c, 0, 0, 0]);
    matrix = matrix.concat([c, c, 0, 0, 0]);
    matrix = matrix.concat([c, c, c, 0, 0]);
    matrix = matrix.concat([0, 0, 0, 1, 0]);

    return ColorMatrix.of(matrix).toFilter();
  }

  @:diExport
  private var GEORGES(default, null): IPatch;
  @:diExport
  private var ICE_SKIN_H(default, null): IPatch;
  @:diExport
  private var ICE_SKIN_V(default, null): IPatch;
  @:diExport
  private var ICE_SKIN_BACK(default, null): IPatch;
  @:diExport
  private var ICE_SKIN_BACK_SPECIAL(default, null): IPatch;
  @:diExport
  private var ICE_SKIN_SIDES(default, null): IPatch;
  @:diExport
  private var ICE_SKIN_STALACTITES(default, null): IPatch;
  // Add support for snow
  private static var SNOW(default, null): IPatch = new PatchList([
    Ref.auto(View.scale).before(function(hf: Hf, self: View, ratio: Float): Void {
      var state: Null<IceState> = getIceState(self.world.manager.current);
      if (state == null) {
        return;
      }
      var scale: Float = Math.round(ratio * 100);
      state.snowMC._xscale = scale;
      state.snowMC._yscale = scale;
    }),
    Ref.auto(ViewManager.onDataReady).after(function(hf: Hf, self: ViewManager): Void {
      var state: Null<IceState> = getIceState(self.manager.current);
      if (state == null) {
        return;
      }
      if (self.fl_scrolling) {
        state.onScroll();
      }
    }),
    Ref.auto(View.moveTo).after(function(hf: Hf, self: View, x: Float, y: Float): Void {
      var state: Null<IceState> = getIceState(self.world.manager.current);
      if (state == null) {
        return;
      }
      state.snowMC._x = x;
      state.snowMC._y = y;
    }),
    Ref.auto(View.setFilter).after(function(hf: Hf, self: View, f: BitmapFilter): Void {
      var state: Null<IceState> = getIceState(self.world.manager.current);
      if (state == null) {
        return;
      }
      state.snowMC.filters = [f];
    }),
    Ref.auto(View.getSnapShot).after(function(hf: Hf, self: View, x: Float, y: Float, res: BitmapData): Void {
      var state: Null<IceState> = getIceState(self.world.manager.current);
      if (state == null) {
        return;
      }
      res.drawMC(state.snowMC, x + 10, y);
    }),
    Ref.auto(Mode.main).after(function(hf: Hf, self: Mode): Void {
      var state: Null<IceState> = getIceState(self);
      if (state == null) {
        return;
      }
      if (!self.fl_lock) {
        state.update();
      }
    })
  ]);
  @:diExport
  public var snow(default, null): IPatch;
  @:diExport
  public var forceIceVar(default, null): IPatch;

  private var customClips(default, null): CustomClips;
  private var loader : HostModuleLoader;

  public function new(customClips: CustomClips, patches: Array<IPatch>, loader: HostModuleLoader): Void {
    this.iceTrigger = new ice.IceTrigger(loader);
    this.activeOption =
      Ref.auto(hf.mode.GameMode.initWorld)
         .after(function(hf, self) : Void{
           if (Ice.isOptionEnabled(loader))
             {
               enableIce(hf, self);
             }
         });
    this.ICE_SKIN_H =
      Ref.auto(View.attachTile)
         .after(function(hf: Hf, self: View, sx: Int, sy: Int, wid: Int, skin: Int): Void {
           if (isOptionEnabled(this.loader)){
             var tile = self.tileList[self.tileList.length - 1];
             tile.filters = [Ice.getBlue()];
             hf.Std.attachMC(Reflect.field(tile, "skin"), "$glace_plateforme",0);
           }
         });
    this.ICE_SKIN_V =
      Ref.auto(View.attachColumn)
         .after(function(hf: Hf, self: View, sx: Int, sy: Int, wid: Int, skin: Int): Void {
           if (isOptionEnabled(this.loader)){
             self.tileList[self.tileList.length - 1].filters = [Ice.getBlue()];
             /*if (self.data.__dollar__map[sx][sy - 1] == 0){
             var tile = self.tileList[self.tileList.length - 1];
             tile.filters = [Ice.getBlue()];
             var mc = hf.Std.attachMC(Reflect.field(tile, "skin"), "$glace_plateforme",0);
             mc._rotation = 270;
             mc._alpha = 50;
             mc._xscale *= -1;
             }*/
           }
         });
    this.ICE_SKIN_BACK =
      Ref.auto(View.attachBg).after(function(hf: Hf, self: View): Void {
        if (isOptionEnabled(this.loader))
          self._bg.filters = [Ice.getBlue()];
      });
    this.ICE_SKIN_BACK_SPECIAL =
      Ref.auto(View.attachSpecialBg)
         .wrap(function(hf: Hf, self: View, id: Int, subId: Null<Int>, old){
           if (isOptionEnabled(this.loader)){
             var bg = old(self, id, subId);
             bg.filters = [Ice.getBlue()];
             return bg;
           }
           return old(self, id, subId);
         });
    this.ICE_SKIN_SIDES =
      Ref.auto(View.display)
         .after(function(hf: Hf, self: View, id : Int): Void {
           if (!Ice.isOptionEnabled(loader))
             return;
           var y = hf.Std.random(500);
           for (i in 0...10){
             y = (y + 50 +  hf.Std.random(75))%500;
             var cube = self.attachSprite("GiftGod", 10,y, false);
             cube._rotation = hf.Std.random(360);
             cube._xscale = 45 + hf.Std.random(45);
             cube._yscale = cube._xscale;
             cube._x -= cube._width/2;
             cube._alpha = 25 + hf.Std.random(75);
           }
           for (i in 0...10){
             y = (y + 50 +  hf.Std.random(75))%500;
             var cube = self.attachSprite("GiftGod", 410, y, false);
             cube._rotation = hf.Std.random(360);
             cube._xscale = 45 + hf.Std.random(45);
             cube._yscale = cube._xscale;
             cube._x += cube._width/2;
             cube._alpha = 25 + hf.Std.random(75);
           }
         });
    this.ICE_SKIN_STALACTITES =
      Ref.auto(hf.levels.View.attach)
         .after(function(hf, self){
           if(self.world.scriptEngine.cycle < 10 && isOptionEnabled(this.loader)){
             for(i in 0...20){
               for(j in 0...23){
                 if (self.data.__dollar__map[i][j] > 0 && self.data.__dollar__map[i][j+1] == 0 && self.data.__dollar__map[i][j+2] == 0 && hf.Std.random(100) < 50){
                   var M = hf.Std.random(4) + 1;
                   for (n_stalactite in 0...M){
                     var s = self.attachSprite("$stalactite", 20*i + 10, 20*j + 20, true);
                     s._x += hf.Std.random(15);
                     s._xscale = (50+hf.Std.random(50));
                     s._yscale = s._xscale;
                     s._alpha = (20+hf.Std.random(80));
                   }
                 }
               }
             }
           }
         });
    this.GEORGES =
      Ref.auto(GameMode.initWorld)
         .after(function(hf: Hf, self: GameMode): Void {
           if (isOptionEnabled(this.loader)){
             hf.Data.HU_STEPS[0] *= 1.25;
             hf.Data.HU_STEPS[1] *= 1.35;
           }
         });
    this.forceIceVar = FORCE_ICE_VAR;
    this.customClips = customClips;
    patches.push(SNOW);
    this.loader = loader;
    this.snow = new PatchList(patches);
  }

  public function enableIce(hf: Hf, _game: Mode): Void {
    var game: GameMode = cast _game;
    var DP_BORDERS: Int = hf.Data.DP_BORDERS;
    var snowMC: MovieClip = game.depthMan.empty(DP_BORDERS);
    var state: IceState = new IceState(hf, customClips, snowMC);
    STATES.set(game, state);
    redraw(game.world.view);
  }

  public function disableIce(game: Mode): Void {
    STATES.remove(game);
    // TODO: Cleanup
  }

  public static function isIce(game: Mode): Bool {
    return STATES.get(game) != null;
  }

  public static function getIceState(game: Mode): Null<IceState> {
    return STATES.get(game);
  }

  private static function redraw(view: View): Void {
    view.detachLevel();
    view.displayCurrent();
    view.moveToPreviousPos();
  }
}
