package ice;

import merlin.IAction;
import merlin.IActionContext;
import patchman.HostModuleLoader;
import etwin.Obfu;

class IceTrigger implements IAction {

  public var name(default, null): String = Obfu.raw("ice");
  public var isVerbose(default, null): Bool = true;
  public var loader : HostModuleLoader;

  public function new(loader: HostModuleLoader) {
    this.loader = loader;
  }

  public function run(ctx: IActionContext): Bool {
    return Ice.isOptionEnabled(loader);
  }
}